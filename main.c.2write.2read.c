
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <unistd.h>
#include <stdlib.h>
#include <getopt.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/types.h>
#include <linux/spi/spidev.h>

/* Include to get access to the VectorNav SPI functions. */
#include "vn/protocol/spi.h"
#include "vn/protocol/common.h"
#include "vn/xplat/thread.h"

int display_error(const char* msg);
void mockspi_initialize(void);
void mockspi_writeread(const char* dataOut, size_t dataOutSize, char* dataIn);
static uint8_t mode;
static uint8_t bits = 8;
static uint32_t speed = 500000;
static uint16_t delay;
char txbuf[128];
char rxbuf[128];


char productname[512];
size_t bufcmdsize;
size_t responseSize;

#define SIZE_OF_FLOAT           4

union {
    float fVal;
    int32_t intVal;
    uint32_t uintVal;
    unsigned char byteVal[4];
} floatAsByte;


static void transfer(int fd )
{
	int ret, times = 0, j = 0;
	int  regId = 5;
	int cmd = 1;
	float yaw, pitch , roll;
	/*
	struct spi_ioc_transfer tr = {
		.tx_buf = (unsigned long)txbuf,
		.rx_buf = (unsigned long)rxbuf,
		.len = responseSize,
		.delay_usecs = delay,
		.speed_hz = speed,
		.bits_per_word = bits,
		.cs_change = 0,
		.delay_usecs = 100,
	};
	*/
	for (;;)
	{
		delay = 100;
		responseSize = 128 ; //sizeof ( rxbuf );
		struct  spi_ioc_transfer tr[2];
		memset ( tr , 0 , sizeof ( struct spi_ioc_transfer ) * 2 );
		memset ( rxbuf , 0 , sizeof ( rxbuf ) );
		memset ( txbuf , 0 , sizeof ( txbuf ) );
		txbuf[0] = (char )cmd;
		txbuf[1] = (char )regId;
		txbuf[2] = 0x00;
		txbuf[3] = 0x00;
			
		printf ( " send buffer \n");	
		
		for (ret = 0; ret < 4; ret++) 
		{
			if (!(ret % 6))
				puts("");
			printf("0x%x ", txbuf[ret]);
		}	
		puts("");
			
		ret = write(fd, txbuf, 4 ); 
		if ( ret < 0 )
			perror("Write Error");
		else
			printf ( " wrote %d byte \n", ret );
			
		VnThread_sleepMs(100);
			
		switch (regId )
		{
			case 0:
			case 1:
			case 4:
			responseSize = 4;
			break;
			
			case 2:
			case 3:
			case 35:
			case 67:
				responseSize = 8;
				break;
			case 5:
			case 6:
			case 55:
				responseSize = 9;
				break;
			case 8:
			case 17:
			case 18:
			case 19:
			case 33:
			case 50:
			case 57:
				responseSize = 16;
				break;
			case 9:
				responseSize = 20;
				break;
				
			case 15:
				responseSize = 56;
				break;
				
			case 20:
			case 26:
			case 36:
			case 38:
			case 239:
			case 240:
				responseSize = 40;
				break;
			case 21:
			case 93:
				responseSize = 28;
				break;
			case 23:
			case 25:
			case 27:
			case 47:
			case 84:
				responseSize = 52;
				break;
				
			case 30:
				responseSize = 11;
				break;
				
			case 32:
				responseSize = 24;
				break;
				
			case 44:
				responseSize = 7;
				break;
				
			case 51:
				responseSize = 13;
				break;
				
			case 54:
				responseSize = 48;
				break;	
				
			case 58:
			case 59:
			case 63:
			case 64:
			case 72:
			case 73:
				responseSize = 76;
				break;
			
			case 74:
			case 80:
				responseSize = 32;
				break;
			
			case 82:
				responseSize = 10;
				break;	
				
			case 85:
				responseSize = 19;
				break;

			case 97:
				responseSize = 32;
				break;
				
			default :	
				responseSize = sizeof ( rxbuf );
				break;

				
		}
		if  ( responseSize < sizeof ( rxbuf ) )
			responseSize+=4;
		#if 0
		memset ( rxbuf , 0 , sizeof ( txbuf ) );
		ret = read (fd, rxbuf, responseSize ); 
		if ( ret < 0 )
			perror("Write Error");
		else
			printf ( " read %d byte line %d\n", ret , __LNE__);
			
		VnThread_sleepMs(100);	
		#endif 
			
		//VnThread_sleepMs(100);
		
		memset ( txbuf , 0 , sizeof ( txbuf ) );
		ret = read (fd, rxbuf, responseSize); 
		if ( ret < 0 )
			perror("Write Error");
		else
		{
			printf ( " wrote %d byte line %d\n", ret, __LINE__ );
			if ( regId == 27 || regId == 126 )
			{  //yaw , pitch roll
				memcpy ( floatAsByte.byteVal, &rxbuf[0+4], SIZE_OF_FLOAT );
				yaw = floatAsByte.fVal;
				
				gcvt(yaw, 6, productname);				
				printf ( "yaw = %s \n", productname );
				
				memcpy ( floatAsByte.byteVal, &rxbuf[4+4], SIZE_OF_FLOAT );
				pitch = floatAsByte.fVal;
				gcvt(pitch, 6, productname);				
				printf ( "pitch = %s \n", productname );
				memcpy ( floatAsByte.byteVal, &rxbuf[8+4], SIZE_OF_FLOAT );
				roll = floatAsByte.fVal;
				gcvt(roll, 6, productname);				
				printf ( "roll = %s \n", productname );
				
				
				
				for (ret = 0; ret < responseSize + 4; ret++) 
				{
					if (!(ret % 6))
					puts("");
					printf("%.2X ", rxbuf [ret]);
				}
			}
			else
			for (ret = 0; ret < responseSize  ; ret++) 
			{
				if (!(ret % 6))
					puts("");
				printf("%.2X ", rxbuf [ret]);
			}	
			puts("");
			
		}
			
		VnThread_sleepMs(100);
		
		
		
		ret = read ( fd , rxbuf, responseSize  ); //sizeof ( rxbuf ) );	
		if (ret < 1 )
			perror("Read Error");
		
		else
		{
			printf ( " \nfirst read %d bytes line %d\n", ret , __LINE__ );
			
			for (ret = 0; ret < responseSize  ; ret++) 
			{
				if (!(ret % 6))
					puts("");
				printf("%.2X ", rxbuf [ret]);
			}
			puts("");			
			
			
		}
		
		VnThread_sleepMs(100);
		ret = read ( fd , rxbuf , responseSize  ); //sizeof ( rxbuf ) );	
		if (ret < 1 )
			perror("Read Error");		
		else
		{
			printf ( " \nsecond read %d bytes line %d\n", ret , __LINE__ );			
			
			if ( regId == 27 || regId == 126 )
			{  //yaw , pitch roll
				memcpy ( floatAsByte.byteVal, &rxbuf[0+4], SIZE_OF_FLOAT );
				yaw = floatAsByte.fVal;
				
				gcvt(yaw, 6, productname);				
				printf ( "yaw = %s \n", productname );
				
				memcpy ( floatAsByte.byteVal, &rxbuf[4+4], SIZE_OF_FLOAT );
				pitch = floatAsByte.fVal;
				gcvt(pitch, 6, productname);				
				printf ( "pitch = %s \n", productname );
				memcpy ( floatAsByte.byteVal, &rxbuf[8+4], SIZE_OF_FLOAT );
				roll = floatAsByte.fVal;
				gcvt(roll, 6, productname);				
				printf ( "roll = %s \n", productname );
				
				
				
				for (ret = 0; ret < responseSize + 4; ret++) 
				{
					if (!(ret % 6))
					puts("");
					printf("%.2X ", rxbuf [ret]);
				}	
			}
			else
			for (ret = 0; ret < responseSize + 4; ret++) 
			{
				if (!(ret % 6))
					puts("");
				printf("%.2X ", rxbuf [ret]);
			}	
			
		}
		VnThread_sleepMs(100);
		
		#if 0	
		ret = read ( fd , rxbuf , sizeof ( rxbuf ) );	
		if (ret < 1 )
			perror("Read Error");		
		else
		{
		   		
			printf ( " \nread %d bytes line %d\n", ret , __LINE__ );
			if ( regId == 27 || regId == 126 )
			{  //yaw , pitch roll
				memcpy ( floatAsByte.byteVal, &rxbuf[0+4], SIZE_OF_FLOAT );
				yaw = floatAsByte.fVal;
				gcvt(yaw, 6, productname);				
				printf ( "yaw = %s \n", productname );
				
				memcpy ( floatAsByte.byteVal, &rxbuf[4+4], SIZE_OF_FLOAT );
				pitch = floatAsByte.fVal;
				
				gcvt(pitch, 6, productname);				
				printf ( "pitch = %s \n", productname );
				memcpy ( floatAsByte.byteVal, &rxbuf[8+4], SIZE_OF_FLOAT );
				roll = floatAsByte.fVal;
				
				gcvt(roll, 6, productname);				
				printf ( "roll = %s \n", productname );
							
				for (ret = 0; ret < 96; ret++) 
				{
					if (!(ret % 6))
					puts("");
					printf("%.2X ", rxbuf [ret]);
				}	
			}
			else
			for (ret = 0; ret < 24; ret++) 
			{
				if (!(ret % 6))
					puts("");
				printf("%.2X ", rxbuf [ret]);
			}
		}
		#endif
		#if 0
		VnThread_sleepMs(100);
		ret = read ( fd , rxbuf , responseSize ) ; //sizeof ( rxbuf ) );	
		if (ret < 1 )
			perror("Read Error");		
		else
		{
			printf ( " \nread %d bytes line %d\n", ret , __LINE__ );
			for (ret = 0; ret < 24; ret++) 
			{
				if (!(ret % 6))
					puts("");
				printf("%.2X ", rxbuf [ret]);
			}	
			
		}
		#endif
		printf ( " \n enter cmd and regid %d times \n", times );
		//scanf ( "%d", &cmd );
		//scanf ( "%d", &regId );
		regId = j;
		cmd = 1;
		
		printf ( " cmd %d , regId %d \n" , cmd , regId );
		
		if ( j == 255 )
			break;
			
		times++;
		j++;
		VnThread_sleepMs(1000);
		
		
	}
}
int main(int argc , char *argv[] )
{
	/* This example walks through using the VectorNav C Library to connect to
	 * and interact with a mock VectorNav sensor through the Serial Peripheral
	 * Interaface (SPI). Once you work through and understand the example, you
	 * may want to try replacing the mock functions with ones that interface
	 * with your SPI subsystem. */

	
	int fd, vnerr , ret;
	char device_name[] = {"/dev/spidev2.0"};
	char atl_device_name[] = {"/dev/spidev2.1"};
	char *dev_open ;
	
	if ( argc == 2 )
		dev_open = atl_device_name;
	else
		dev_open = device_name;

	printf ( " opening %s \n", dev_open );
	
	fd = open ( dev_open , O_RDWR );
	if ( fd < 0)
	{
		printf ( " cannot open device %s \n", dev_open );
		perror("failed to open spi device");
		abort();
	}
		/*
	 * spi mode
	 */
	ret = ioctl(fd, SPI_IOC_WR_MODE, &mode);
	if (ret == -1)
		perror("can't set spi mode");

	ret = ioctl(fd, SPI_IOC_RD_MODE, &mode);
	if (ret == -1)
		perror("can't get spi mode");

	/*
	 * bits per word
	 */
	ret = ioctl(fd, SPI_IOC_WR_BITS_PER_WORD, &bits);
	if (ret == -1)
		perror ("can't set bits per word");

	ret = ioctl(fd, SPI_IOC_RD_BITS_PER_WORD, &bits);
	if (ret == -1)
		perror ("can't get bits per word");

	/*
	 * max speed hz
	 */
	ret = ioctl(fd, SPI_IOC_WR_MAX_SPEED_HZ, &speed);
	if (ret == -1)
		perror("can't set max speed hz");

	ret = ioctl(fd, SPI_IOC_RD_MAX_SPEED_HZ, &speed);
	if (ret == -1)
		perror("can't get max speed hz");

	printf("spi mode: %d\n", mode);
	printf("bits per word: %d\n", bits);
	printf("max speed: %d Hz (%d KHz)\n", speed, speed/1000);

	bufcmdsize = sizeof ( txbuf );
	

	vnerr = VnSpi_genReadModelNumber(txbuf , &bufcmdsize, sizeof (txbuf ), &responseSize); 
	if ( vnerr != 0 )
	{
		printf ( "VnSpi_genRead_ModelNumber failed \n");
		perror ( "model number failed");
		abort ();
	}
	transfer (fd );
	
   	//vnerr = VnSpi_parseModelNumber( rxbuf  , productname , sizeof ( productname ));
  
	//vnerr = VnSpi_genReadHardwareRevision(txbuf , &bufcmdsize ,sizeof (txbuf ), &responseSize);
 	//ret = ioctl( fd , SPI_IOC_MESSAGE(1), rxbuf );
        //if (ret == -1)
	//{
	//	perror ("can't read spi port" );
	//        abort();
	//}

	 //vnerr = VnSpi_genReadSerialNumber(txbuf ,  &bufcmdsize ,sizeof (txbuf ), &responseSize ) ; 
        //ret = ioctl( fd , SPI_IOC_MESSAGE(1), rxbuf );
        //if (ret == -1)
        //{//
        //          perror ("can't read spi port" );
        //        abort();
        //}

	//mockspi_initialize();

	/* With SPI 'initialize', let's work through reading the current yaw, pitch,
	 * roll values from the sensor. */

	/* First we must generate the command to query the sensor. */
	//bufcmdsize = sizeof(txbuf);		
	/* First set this variable to the size of the buffer. */
	//if (VnSpi_genReadYawPitchRoll(
	//	txbuf,
	//	&bufcmdsize,				/* Pass in the pointer since the function will set this with the length of the command generate. */
		//0,
		//&responseSize) != E_NONE)
		//return display_error("Error generating read yaw, pitch, roll command.\n");

	/* Send out the command over SPI. */
	//mockspi_writeread(
	//	txbuf,
//		responseSize,
//		rxbuf);

	/* Now the sensor will have responded with data on this transaction but
	 * since the sensor only responds on following transaction, we will
	 * disregard this data. These double transactions can be mitigated by only
	 * requesting the same data each time or by staggering the the requested
	 * data in an appropriate order. */

	/* Make sure enough time has passed for the sensor to format the previous response. */
	VnThread_sleepMs(1);	/* Actual sensor requirement is only 50 us. */

	/* Retransmit so the sensor responds with the previous request. */
	///mockspi_writeread(
	//	txbuf,
	//	responseSize,
	///	rxbuf);

	/* Now parse the received response. */
	//if (VnSpi_parseYawPitchRoll(
//		rxbuf,
//		&ypr) != E_NONE)
//		return display_error("Error parsing yaw, pitch, roll.\n");

//	str_vec3f(strConversions, ypr);
//	printf("Current YPR: %s\n", strConversions);

	/* We have now shown how to process one full command transaction which
	 * requires two SPI transactions because the VectorNav sensor requires a
	 * short amount of time to ready the response. Now we can optimize this
	 * transaction squence to utilize this behavior when we are only requesting
	 * the same data each time. This is illustrated in the for loop below. */

	return 0;
}

int display_error(const char* msg)
{
	printf("%s\n", msg);
	return -1;
}

void mockspi_initialize(void)
{
	/* Do nothing since we are faking the SPI interface. */
}

void mockspi_writeread(const char* dataOut, size_t dataOutSize, char* dataIn)
{
	/* This function fakes a SPI subsystem for this example. */

	char yprResponse[] = { 0x00, 0x01, 0x08, 0x00, 0xd8, 0x9c, 0xd4, 0x42, 0x44, 0xba, 0x9e, 0x40, 0x4e, 0xe4, 0x8b, 0x40 };

	/* Silence 'unreferenced formal parameters' warning in Visual Studio. */
	//(dataOut);
	//(dataOutSize);

	memcpy(dataIn, yprResponse, sizeof(yprResponse));
}
