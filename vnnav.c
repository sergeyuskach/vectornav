/*
*	Sergey Uskach
*	3/8/2021
*   initial version. User space driver for vectornav hardware
*   vnnav.c
*
*/
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <unistd.h>
#include <stdlib.h>
#include <getopt.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/types.h>
#include <linux/spi/spidev.h>

int display_error(const char* msg);
char *device_to_open ( void );

static uint8_t mode;
static uint8_t bits = 8;
static uint32_t speed = 8000000; 
char txbuf[128];
char rxbuf[128];


char productname[256];

size_t responseSize;

#define SIZE_OF_FLOAT           4
#define SIZE_OF_DOUBLE			8
#define READ_VN_REGISTER		1
#define READ_YAW_ROLL_PITCH		8
#define READ_Y_R_P_G 			27
#define READ_Y_R_P_AL_LAT_LON   63
#define READ_AL_LAT_LON_NED		58
#define VELOCITY_X_Y_Z			72

char device_name[] = {"/dev/spidev2.0"};
char second_dev_name[] = {"/dev/spidev1.0"};
char *dev_open = NULL;
int fd,  ret;
	

union {
    float fVal;
    int32_t intVal;
    uint32_t uintVal;
    unsigned char byteVal[4];
} floatAsByte;

union
{
	char arr[8];
	double d;
}doubletobyte;
int get_yaw_roll_pitch ( float *yaw, float *roll, float *pithch );


static char read_size ( int regId )
{
	char responseSize;
	
		switch (regId )
		{
			
			case 1:
			case 21:
			case 93:
				responseSize = 24;
				break;
				
			
			case 0:
			case 2:
			case 3:
			case 4:
			case 5:
			case 6:
			case 7:
			case 35:
			case 44:
			case 67:
			case 98:
			case 101:
			case 102:
				responseSize = 4;
				break;
					
			case 55:
				responseSize = 5;
				break;
				
			case 8:
			case 17:
			case 18:
			case 19:
			case 33:
			case 50:
			case 57:
				responseSize = 12;
				break;
				
			case 32:
				responseSize = 20;
				break;
				
			case 15:
				responseSize = 52;
				break;
				
			case 20:
			case 26:
			case 36:
			case 38:
			case 239:
			case 240:
				responseSize = 36;
				break;
			
			case 23:
			case 25:
			case 27:
			case 47:
			case 84:
				responseSize = 48;
				break;
				
			case 30:
				responseSize = 7;
				break;							
				
			case 51:
				responseSize = 13;
				break;
				
			case 54:
				responseSize = 44;
				break;	
				
			case 58:
			case 59:
			case 63:
			case 64:
			case 103:
			case 104:
				responseSize = 72;
				break;
							
			case 72:
			case 73:
				responseSize = 80;
				break;
			
			case 80:
			case 74:	
			case 97:
				responseSize = 28;
				break;
			
			case 83:
			case 86:
				responseSize = 32;
				break;
			
			case 82:
				responseSize = 6;
				break;	
				
			case 85:
				responseSize = 15;
				break;
				
			case 9:
			case 100:
				responseSize = 16;
				break;		

			case 75:
			case 76:
			case 77:
				responseSize = 22;
				break;
				
			default :	
				responseSize = sizeof ( rxbuf );
				break;
		
		}
	return responseSize;
	
}

int get_yaw_roll_pitch ( float *yaw, float *roll, float *pitch )
{
	char responseSize ;
	int fd, ret;
	
	fd = open ( device_to_open() , O_RDWR );
	if ( fd < 0)
	{
		printf ( " cannot open device %s \n", device_name );
		perror("failed to open spi device");
		abort();
	}
	
	/*
	 * spi mode
	*/
	mode = SPI_MODE_0; 
	mode |= SPI_CPOL;
	mode |= SPI_CPHA;
	  
	ret = ioctl(fd, SPI_IOC_WR_MODE, &mode);
	if (ret == -1)
	{
		perror("can't set spi mode");
		return ret;
	}
	/*
	 * bits per word
	 */
	ret = ioctl(fd, SPI_IOC_WR_BITS_PER_WORD, &bits);
	if (ret == -1)
	{
		perror ("can't set bits per word");
		return ret;
	}
	/*
	 * max speed hz
	 */
	ret = ioctl(fd, SPI_IOC_WR_MAX_SPEED_HZ, &speed);
	if (ret == -1)
	{
		perror("can't set max speed hz");
		return ret;
	}
		
	responseSize = read_size ( READ_YAW_ROLL_PITCH );
	
	memset ( rxbuf , 0 , sizeof ( rxbuf ) );
	memset ( txbuf , 0 , sizeof ( txbuf ) );
	txbuf[0] = (char )READ_VN_REGISTER;
	txbuf[1] = (char )READ_YAW_ROLL_PITCH;
	txbuf[2] = 0x00;
	txbuf[3] = 0x00;
	
	if  ( responseSize < sizeof ( rxbuf ) )
			responseSize+=4;
			
	ret = write(fd, txbuf, responseSize ); 
	if ( ret < 0 )
	{
		perror("Write Error");
		return ret;
	}
	
			
	//VnThread_sleepMs(100);
	usleep(100 * 1000);
	
	memset ( rxbuf , 0 , sizeof ( rxbuf ) );
		
	ret = read (fd, rxbuf, responseSize); 
		
	if ( ret < 0 )
	{
		perror("read Error");
		return ret;
	}
		
	memcpy ( floatAsByte.byteVal, &rxbuf[0+4], SIZE_OF_FLOAT );
	*yaw = floatAsByte.fVal;				
					
	memcpy ( floatAsByte.byteVal, &rxbuf[4+4], SIZE_OF_FLOAT );
	*pitch = floatAsByte.fVal;
	
	memcpy ( floatAsByte.byteVal, &rxbuf[8+4], SIZE_OF_FLOAT );
	*roll = floatAsByte.fVal;
	close ( fd ); 
	return 0;
}

int get_yaw_roll_pitch_acel_gyro ( float *yaw, float *roll, float *pitch , float *magx, float *magy,float  *magz, float *gyrox, float *gyroy, float *gyroz)
{
	char responseSize ;
	int fd, ret;
	
	fd = open ( device_to_open() , O_RDWR );
	if ( fd < 0)
	{
		printf ( " cannot open device %s \n", device_name );
		perror("failed to open spi device");
		abort();
	}
	
	/*
	 * spi mode
	*/
	mode = SPI_MODE_0; 
	mode |= SPI_CPOL;
	mode |= SPI_CPHA;
	  
	ret = ioctl(fd, SPI_IOC_WR_MODE, &mode);
	if (ret == -1)
	{
		perror("can't set spi mode");
		return ret;
	}
	/*
	 * bits per word
	 */
	ret = ioctl(fd, SPI_IOC_WR_BITS_PER_WORD, &bits);
	if (ret == -1)
	{
		perror ("can't set bits per word");
		return ret;
	}
	/*
	 * max speed hz
	 */
	ret = ioctl(fd, SPI_IOC_WR_MAX_SPEED_HZ, &speed);
	if (ret == -1)
	{
		perror("can't set max speed hz");
		return ret;
	}
		
	responseSize = read_size ( READ_Y_R_P_G );
	
	memset ( rxbuf , 0 , sizeof ( rxbuf ) );
	memset ( txbuf , 0 , sizeof ( txbuf ) );
	txbuf[0] = (char )READ_VN_REGISTER;
	txbuf[1] = (char )READ_Y_R_P_G;
	txbuf[2] = 0x00;
	txbuf[3] = 0x00;
	
	if  ( responseSize < sizeof ( rxbuf ) )
			responseSize+=4;
			
	ret = write(fd, txbuf, responseSize ); 
	if ( ret < 0 )
	{
		perror("Write Error  ");
		return ret;
	}

	usleep(100 * 1000);
	
	memset ( rxbuf , 0 , sizeof ( rxbuf ) );
		
	ret = read (fd, rxbuf, responseSize); 
		
	if ( ret < 0 )
	{
		perror("read Error");
		return ret;
	}
		
	memcpy ( floatAsByte.byteVal, &rxbuf[0+4], SIZE_OF_FLOAT );
	*yaw = floatAsByte.fVal;				
				
	memcpy ( floatAsByte.byteVal, &rxbuf[4+4], SIZE_OF_FLOAT );
	*pitch = floatAsByte.fVal;
				
	memcpy ( floatAsByte.byteVal, &rxbuf[8+4], SIZE_OF_FLOAT );
	*roll = floatAsByte.fVal;
	
	memcpy ( floatAsByte.byteVal, &rxbuf[12+4], SIZE_OF_FLOAT );
	*magx = floatAsByte.fVal;
	
	memcpy ( floatAsByte.byteVal, &rxbuf[16+4], SIZE_OF_FLOAT );
	*magy = floatAsByte.fVal;
				
	memcpy ( floatAsByte.byteVal, &rxbuf[20+4], SIZE_OF_FLOAT );
	*magz = floatAsByte.fVal;
				
	memcpy ( floatAsByte.byteVal, &rxbuf[24+4], SIZE_OF_FLOAT );
	*gyrox = floatAsByte.fVal;
	
	memcpy ( floatAsByte.byteVal, &rxbuf[28+4], SIZE_OF_FLOAT );
	*gyroy = floatAsByte.fVal;
	
	memcpy ( floatAsByte.byteVal, &rxbuf[32+4], SIZE_OF_FLOAT );
	*gyroz = floatAsByte.fVal;
	
	close ( fd ); 
	return 0;
}


int get_yaw_roll_pitch_alt_lat_log ( float *yaw, float *roll, float *pitch , double *alt, double *lat, double  *lon, float *nedx, float *nedy, float *nedz)
{
	char responseSize ;
	int fd, ret;
	
	fd = open ( device_to_open(), O_RDWR );
	if ( fd < 0)
	{
		printf ( " cannot open device %s \n", device_name );
		perror("failed to open spi device");
		abort();
	}
	
	/*
	 * spi mode
	*/
	mode = SPI_MODE_0; 
	mode |= SPI_CPOL;
	mode |= SPI_CPHA;
	  
	ret = ioctl(fd, SPI_IOC_WR_MODE, &mode);
	if (ret == -1)
	{
		perror("can't set spi mode");
		return ret;
	}
	/*
	 * bits per word
	 */
	ret = ioctl(fd, SPI_IOC_WR_BITS_PER_WORD, &bits);
	if (ret == -1)
	{
		perror ("can't set bits per word");
		return ret;
	}
	/*
	 * max speed hz
	 */
	ret = ioctl(fd, SPI_IOC_WR_MAX_SPEED_HZ, &speed);
	if (ret == -1)
	{
		perror("can't set max speed hz");
		return ret;
	}
		
	responseSize = read_size ( READ_AL_LAT_LON_NED );
	
	memset ( rxbuf , 0 , sizeof ( rxbuf ) );
	memset ( txbuf , 0 , sizeof ( txbuf ) );
	txbuf[0] = (char )READ_VN_REGISTER;
	txbuf[1] = (char )READ_AL_LAT_LON_NED;
	txbuf[2] = 0x00;
	txbuf[3] = 0x00;
	
	if  ( responseSize < sizeof ( rxbuf ) )
			responseSize+=4;
			
	ret = write(fd, txbuf, responseSize ); 
	if ( ret < 0 )
	{
		perror("Write Error \n ");
		return ret;
	}

	usleep(100 * 1000);
	
	memset ( rxbuf , 0 , sizeof ( rxbuf ) );
		
	ret = read (fd, rxbuf, responseSize); 
		
	if ( ret < 0 )
	{
		perror("read Error");
		return ret;
	}
	
	memcpy ( floatAsByte.byteVal, &rxbuf[12+4], SIZE_OF_FLOAT );
	*yaw = floatAsByte.fVal;				
				
	memcpy ( floatAsByte.byteVal, &rxbuf[16+4], SIZE_OF_FLOAT );
	*pitch = floatAsByte.fVal;
				
	memcpy ( floatAsByte.byteVal, &rxbuf[20+4], SIZE_OF_FLOAT );
	*roll = floatAsByte.fVal;
	
	memcpy ( &doubletobyte.arr[0], &rxbuf[24+4] , SIZE_OF_DOUBLE );
	*lat = doubletobyte.d ;
	
	memcpy ( &doubletobyte.arr[0], &rxbuf[32+4] , SIZE_OF_DOUBLE );
	*lon = doubletobyte.d ;
	
	memcpy ( &doubletobyte.arr[0], &rxbuf[40+4] , SIZE_OF_DOUBLE );
	*alt = doubletobyte.d ;
	
	memcpy ( floatAsByte.byteVal, &rxbuf[48+4], SIZE_OF_FLOAT );
	*nedx = floatAsByte.fVal;
	
	memcpy ( floatAsByte.byteVal, &rxbuf[52+4], SIZE_OF_FLOAT );
	*nedy = floatAsByte.fVal;
	
	memcpy ( floatAsByte.byteVal, &rxbuf[56+4], SIZE_OF_FLOAT );
	*nedz = floatAsByte.fVal;
	
	close ( fd ); 
	return 0;
}

int get_alt_lat_lon_ned ( double *alt, double *lon,  double *lat, float *velx, float *vely, float *velz )
{
	char responseSize ;
	int fd, ret;
	
	fd = open ( device_to_open () , O_RDWR );
	if ( fd < 0)
	{
		printf ( " cannot open device %s \n", device_name );
		perror("failed to open spi device");
		abort();
	}
	
	/*
	 * spi mode
	*/
	mode = SPI_MODE_0; 
	mode |= SPI_CPOL;
	mode |= SPI_CPHA;
	  
	ret = ioctl(fd, SPI_IOC_WR_MODE, &mode);
	if (ret == -1)
	{
		perror("can't set spi mode");
		return ret;
	}
	/*
	 * bits per word
	 */
	ret = ioctl(fd, SPI_IOC_WR_BITS_PER_WORD, &bits);
	if (ret == -1)
	{
		perror ("can't set bits per word");
		return ret;
	}
	/*
	 * max speed hz
	 */
	ret = ioctl(fd, SPI_IOC_WR_MAX_SPEED_HZ, &speed);
	if (ret == -1)
	{
		perror("can't set max speed hz");
		return ret;
	}
		
	responseSize = read_size ( VELOCITY_X_Y_Z );
	
	memset ( rxbuf , 0 , sizeof ( rxbuf ) );
	memset ( txbuf , 0 , sizeof ( txbuf ) );
	txbuf[0] = (char )READ_VN_REGISTER;
	txbuf[1] = (char )VELOCITY_X_Y_Z;
	txbuf[2] = 0x00;
	txbuf[3] = 0x00;
	
	if  ( responseSize < sizeof ( rxbuf ) )
			responseSize+=4;
			
	ret = write(fd, txbuf, responseSize ); 
	if ( ret < 0 )
	{
		perror("Write Error");
		return ret;
	}
			
	usleep(100 * 1000);
	
	memset ( rxbuf , 0 , sizeof ( rxbuf ) );
		
	ret = read (fd, rxbuf, responseSize); 
		
	if ( ret < 0 )
	{
		perror("read Error");
		return ret;
	}

	memcpy ( &doubletobyte.arr[0], &rxbuf[16+4] , SIZE_OF_DOUBLE );
	*lat = doubletobyte.d ;
	
	memcpy ( &doubletobyte.arr[0], &rxbuf[24+4] , SIZE_OF_DOUBLE );
	*lon = doubletobyte.d ;
	
	memcpy ( &doubletobyte.arr[0], &rxbuf[32+4] , SIZE_OF_DOUBLE );
	*alt = doubletobyte.d ;
	
	memcpy ( floatAsByte.byteVal, &rxbuf[40+4], SIZE_OF_FLOAT );
	*velx = floatAsByte.fVal;
	
	memcpy ( floatAsByte.byteVal, &rxbuf[44+4], SIZE_OF_FLOAT );
	*vely = floatAsByte.fVal;
	
	memcpy ( floatAsByte.byteVal, &rxbuf[48+4], SIZE_OF_FLOAT );
	*velz = floatAsByte.fVal;
	
	close ( fd ); 
	return 0;
			
}

int display_error(const char* msg)
{
	printf("%s\n", msg);
	return -1;
}

char *device_to_open ( void )
{
	FILE * file;
	file = fopen("/dev/spidev2.0", "r");
	if (file)
	{
		dev_open = device_name;
		fclose(file);
	}
	else
	{
		//file doesn't exists or cannot be opened (es. you don't have access permission)
		dev_open = second_dev_name ;	
	}
	return dev_open;
}

