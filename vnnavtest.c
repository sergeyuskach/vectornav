
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <unistd.h>
#include <stdlib.h>
#include <getopt.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/types.h>
#include <linux/spi/spidev.h>
#include "vnnav.h"






size_t responseSize;

char productname[512];

#define SIZE_OF_FLOAT           4
#define SIZE_OF_DOUBLE			8

union {
    float fVal;
    int32_t intVal;
    uint32_t uintVal;
    unsigned char byteVal[4];
} floatAsByte;

union
{
	char arr[8];
	double d;
}doubletobyte;


int main(void )
{
	int ret = 0;
	float yaw, roll, pitch;
	float magx,magy,magz, gyrox, gyroy, gyroz;
	double alt, lat, lon;
	float nedx, nedy, nedz;
	
	printf ( " testing vectornav calls libraries functions \n");
	
	ret = get_yaw_roll_pitch ( &yaw,  &roll, &pitch );
	
	printf ( " get_yaw_roll_pitch return %d \n", ret );
	
	gcvt(yaw, 6, productname);				
	printf ( "yaw = %s \n", productname );
	
	gcvt(pitch, 6, productname);				
	printf ( "pitch = %s \n", productname );
	
	gcvt(roll, 6, productname);				
	printf ( "roll = %s \n", productname );
	
	printf ( "hit ay key to run next test ...\n");
	
	getchar ();
	
	printf ( "second test \n");
	
	ret = get_yaw_roll_pitch_acel_gyro ( &yaw, &roll, &pitch , &magx, &magy, &magz, &gyrox, &gyroy, &gyroz);
	
	printf ( " get_yaw_roll_pitch_acel_gyro %d \n", ret );
				
	gcvt(yaw, 6, productname);				
	printf ( "yaw = %s \n", productname );
	
	gcvt(pitch, 6, productname);				
	printf ( "pitch = %s \n", productname );
	
	gcvt(roll, 6, productname);				
	printf ( "roll = %s \n", productname );
	
	gcvt(magx, 6, productname);				
	printf ( "magx = %s \n", productname );
	
	gcvt(magy, 6, productname);				
	printf ( "pitch = %s \n", productname );

	gcvt(magz, 6, productname);				
	printf ( "magz = %s \n", productname );
	
	gcvt(gyrox, 6, productname);				
	printf ( "gyrox = %s \n", productname );
	
	
	gcvt(gyroy, 6, productname);				
	printf ( "gyroy = %s \n", productname );
	
	gcvt(gyroz, 6, productname);				
	printf ( "gyroz = %s \n", productname );
	
	printf ( "hit ay key to run next test ...\n");
	
	getchar ();

	ret = get_yaw_roll_pitch_alt_lat_log ( &yaw, &roll, &pitch , &alt, &lat, &lon, &nedx, &nedy, &nedz);
	
	printf ( " get_yaw_roll_pitch_alt_lat_log %d \n", ret );
	gcvt(yaw, 6, productname);				
	printf ( "yaw = %s \n", productname );
	
	gcvt(pitch, 6, productname);				
	printf ( "pitch = %s \n", productname );
	
	gcvt(roll, 6, productname);				
	printf ( "roll = %s \n", productname );
	
	snprintf(productname, sizeof(productname), "%g", lat);
	printf ( "latitude  = %s \n", productname );
	
	snprintf(productname, sizeof(productname), "%g", lon);
	printf ( "longtitude   = %s \n", productname );
				
	snprintf(productname, sizeof(productname), "%g", alt);
	printf ( "altutude  = %s \n", productname );
	
	gcvt(nedx, 6, productname);				
	printf ( "nedx = %s \n", productname );
	
	gcvt(nedy, 6, productname);				
	printf ( "nedy = %s \n", productname );
	
	gcvt(nedz, 6, productname);				
	printf ( "nedz = %s \n", productname );
	
	
	
	
	return 0;
}

