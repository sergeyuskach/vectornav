CC = gcc
INCLUDES = -I ../../include
CFLAGS = -g -Wall

all: spi

clean:
	rm -f main.o
	rm -f spi
	cd ../.. && make clean

spi: main.o 
	$(CC) -g -o spi main.o 

main.o: main.c
	$(CC) $(CFLAGS) $(INCLUDES) -c $< -o $@

