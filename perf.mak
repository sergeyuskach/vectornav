CC = gcc
INCLUDES = -I ../../include
CFLAGS = -g -Wall

all: perfspi

clean:
	rm -f perftest.o
	rm -f perfspi
	
perfspi: perftest.o 
	$(CC) -g -o perfspi perftest.o 

perftest.o: perftest.c
	$(CC) $(CFLAGS) $(INCLUDES) -c $< -o $@

