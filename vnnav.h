/*
*	Sergey Uskach
*	3/8/2021
*   initial version. User space driver for vectornav hardware
*   vnnav.h
*
*/


int display_error(const char* msg);

int get_yaw_roll_pitch ( float *yaw, float *roll, float *pithch );

int get_yaw_roll_pitch ( float *yaw, float *roll, float *pitch );

int get_yaw_roll_pitch_acel_gyro ( float *yaw, float *roll, float *pitch , float *magx, float *magy,float  *magz, float *gyrox, float *gyroy, float *gyroz);

int get_yaw_roll_pitch_alt_lat_log ( float *yaw, float *roll, float *pitch , double *alt, double *lat, double  *lon, float *nedx, float *nedy, float *nedz);




