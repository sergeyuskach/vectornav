CC = gcc
INCLUDES = -I ../../include
CFLAGS = -g -Wall -fPIC

all: vnnavtest  libvnnav.so

clean:
	rm -f vnnavtest.o
	rm -f vnnavtest
	rm -f vnnav.o
	rm -f libvnnav.so

vnnavtest: vnnavtest.o 
	$(CC) -g -o vnavtest vnnavtest.o -L. -lvnnav

vnnavtest.o: vnnavtest.c
	$(CC) $(CFLAGS) $(INCLUDES) -c $< -o $@
	
vnnav.so: 	vnnav.o 
	$(CC) -shared -o libvnvav.so vnnav.o 

vnnav.o: 	vnnav.c
	$(CC) $(CFLAGS) $(INCLUDES) -c $< -o $@


