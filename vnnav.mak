CC = gcc
INCLUDES = -I ../../include
CFLAGS = -g -Wall -fPIC


all:  libvnnav.so
clean:
	rm -f vnnav.o

#gcc -shared -o libhello.so -fPIC hello.c

libvnnav.so: 	vnnav.o 
	$(CC) -shared -o libvnnav.so vnnav.o 

vnnav.o: 	vnnav.c
	$(CC) $(CFLAGS) $(INCLUDES) -c $< -o $@

