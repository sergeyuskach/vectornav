CC = gcc
INCLUDES = -I ../../include
CFLAGS = -g -Wall -fPIC

all: vnavtest  libvnnav.so

clean:
	rm -f vnavtest.o
	rm -f vnavtest
	rm -f vnnav.o
	rm -f libvnnav.so

vnavtest: vnavtest.o 
	$(CC) -g -o vnavtest vnavtest.o -L. -lvnnav

vnavtest.o: vnavtest.c
	$(CC) $(CFLAGS) $(INCLUDES) -c $< -o $@
	
vnnav.so: 	vnnav.o 
	$(CC) -shared -o libvnvav.so vnnav.o 

vnnav.o: 	vnnav.c
	$(CC) $(CFLAGS) $(INCLUDES) -c $< -o $@


