
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <unistd.h>
#include <stdlib.h>
#include <getopt.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/types.h>
#include <linux/spi/spidev.h>

int display_error(const char* msg);

static uint8_t mode;
static uint8_t bits = 8;
static uint32_t speed = 8000000; //500000;

char txbuf[128];
char rxbuf[128];


char productname[512];

size_t responseSize;

#define SIZE_OF_FLOAT           4
#define SIZE_OF_DOUBLE			8

union {
    float fVal;
    int32_t intVal;
    uint32_t uintVal;
    unsigned char byteVal[4];
} floatAsByte;

union
{
	char arr[8];
	double d;
}doubletobyte;


static void transfer(int fd )
{
	int ret, times = 0, j = 0;
	int  regId = 63;
	int cmd = 1;
	int delay = 10000;
	float yaw, pitch , roll, NedVelX, NedVelY, NedVelZ; 
		
		
	for (;;)
	{
		
		responseSize = 128 ; //sizeof ( rxbuf );
		memset ( rxbuf , 0 , sizeof ( rxbuf ) );
		memset ( txbuf , 0 , sizeof ( txbuf ) );
		txbuf[0] = (char )cmd;
		txbuf[1] = (char )regId;
		txbuf[2] = 0x00;
		txbuf[3] = 0x00;
		
		
#ifdef DEBUG			
		printf ( " send buffer \n");	
	
		for (ret = 0; ret < 4; ret++) 
		{
			if (!(ret % 6))
				puts("");
			printf("0x%x ", txbuf[ret]);
		}	
		puts("");
		
		switch (regId )
		{
			
			case 1:
			case 21:
			case 93:
				responseSize = 24;
				break;
				
			
			case 0:
			case 2:
			case 3:
			case 4:
			case 5:
			case 6:
			case 7:
			case 35:
			case 44:
			case 67:
			case 98:
			case 101:
			case 102:
				responseSize = 4;
				break;
					
			case 55:
				responseSize = 5;
				break;
				
			case 8:
			case 17:
			case 18:
			case 19:
			case 33:
			case 50:
			case 57:
				responseSize = 12;
				break;
				
			case 32:
				responseSize = 20;
				break;
				
			case 15:
				responseSize = 52;
				break;
				
			case 20:
			case 26:
			case 36:
			case 38:
			case 239:
			case 240:
				responseSize = 36;
				break;
			
			case 23:
			case 25:
			case 27:
			case 47:
			case 84:
				responseSize = 48;
				break;
				
			case 30:
				responseSize = 7;
				break;							
				
			case 51:
				responseSize = 13;
				break;
				
			case 54:
				responseSize = 44;
				break;	
				
			case 58:
			case 59:
			case 63:
			case 64:
			case 103:
			case 104:
				responseSize = 72;
				break;
							
			case 72:
			case 73:
				responseSize = 80;
				break;
			
			case 80:
			case 74:	
			case 97:
				responseSize = 28;
				break;
			
			case 83:
			case 86:
				responseSize = 32;
				break;
			
			case 82:
				responseSize = 6;
				break;	
				
			case 85:
				responseSize = 15;
				break;
				
			case 9:
			case 100:
				responseSize = 16;
				break;		

			case 75:
			case 76:
			case 77:
				responseSize = 22;
				break;
				
			default :	
				responseSize = sizeof ( rxbuf );
				break;
		
		}
#endif
		responseSize  =  72 ;
		if  ( responseSize < sizeof ( rxbuf ) )
			responseSize+=4;
		
		ret = write(fd, txbuf, responseSize ); 
		if ( ret < 0 )
			perror("Write Error");
#ifdef DEBUG	
		else
			printf ( " wrote %d byte \n", ret );
#endif	
		
		usleep(delay  * 100);
		
		memset ( rxbuf , 0 , sizeof ( rxbuf ) );
		
		ret = read (fd, rxbuf, responseSize); 
		if ( rxbuf[3] != 0x00 )
		{
			for (ret = 0; ret < 4; ret++) 
			{
			if (!(ret % 6))
				puts("");
			printf("0x%x ", rxbuf[ret]);
			}	
			puts("");
			printf ( " vnav return error \n");			
		}
		#if 0
		if ( rxbuf[0] != 0 && rxbuf[1] != 1 && rxbuf[2] != 63 && rxbuf[3] != 0x00 )
		{
			for (ret = 0; ret < 4; ret++) 
			{
			if (!(ret % 6))
				puts("");
			printf("0x%x ", rxbuf[ret]);
			}	
			puts("");
			printf ( " vnav return error \n");
			
		}
		#endif
		
		if ( ret < 0 )
			perror("read Error");
		else
		{
			if ( delay%100 == 0 )
				printf ( "read %d byte delay=%d rxbuf[3] = %d \n", ret, delay, rxbuf[3] );
			
			if ( regId == 27  )
			{  
				memcpy ( floatAsByte.byteVal, &rxbuf[0+4], SIZE_OF_FLOAT );
				yaw = floatAsByte.fVal;				
				gcvt(yaw, 6, productname);				
				printf ( "yaw = %s \n", productname );
				memcpy ( floatAsByte.byteVal, &rxbuf[4+4], SIZE_OF_FLOAT );
				pitch = floatAsByte.fVal;
				gcvt(pitch, 6, productname);				
				printf ( "pitch = %s \n", productname );
				memcpy ( floatAsByte.byteVal, &rxbuf[8+4], SIZE_OF_FLOAT );
				roll = floatAsByte.fVal;
				gcvt(roll, 6, productname);				
				printf ( "roll = %s \n", productname );
				
				memcpy ( floatAsByte.byteVal, &rxbuf[12+4], SIZE_OF_FLOAT );
				roll = floatAsByte.fVal;
				gcvt(roll, 6, productname);				
				printf ( "magx = %s \n", productname );
				
				memcpy ( floatAsByte.byteVal, &rxbuf[16+4], SIZE_OF_FLOAT );
				roll = floatAsByte.fVal;
				gcvt(roll, 6, productname);				
				printf ( "magy = %s \n", productname );
				
				memcpy ( floatAsByte.byteVal, &rxbuf[20+4], SIZE_OF_FLOAT );
				roll = floatAsByte.fVal;
				gcvt(roll, 6, productname);				
				printf ( "magz = %s \n", productname );
				
				memcpy ( floatAsByte.byteVal, &rxbuf[24+4], SIZE_OF_FLOAT );
				roll = floatAsByte.fVal;
				gcvt(roll, 6, productname);				
				printf ( "Accelx = %s \n", productname );
				
				memcpy ( floatAsByte.byteVal, &rxbuf[28+4], SIZE_OF_FLOAT );
				roll = floatAsByte.fVal;
				gcvt(roll, 6, productname);				
				printf ( "Accely = %s \n", productname );
				memcpy ( floatAsByte.byteVal, &rxbuf[32+4], SIZE_OF_FLOAT );
				roll = floatAsByte.fVal;
				gcvt(roll, 6, productname);				
				printf ( "Accelz = %s \n", productname );
				
				memcpy ( floatAsByte.byteVal, &rxbuf[36+4], SIZE_OF_FLOAT );
				roll = floatAsByte.fVal;
				gcvt(roll, 6, productname);				
				printf ( "gyroX = %s \n", productname );
				
				memcpy ( floatAsByte.byteVal, &rxbuf[40+4], SIZE_OF_FLOAT );
				roll = floatAsByte.fVal;
				gcvt(roll, 6, productname);				
				printf ( "gyroY = %s \n", productname );
				
				memcpy ( floatAsByte.byteVal, &rxbuf[44+4], SIZE_OF_FLOAT );
				roll = floatAsByte.fVal;
				gcvt(roll, 6, productname);				
				printf ( "gyroZ = %s \n", productname );
	
				
				for (ret = 0; ret < responseSize ; ret++) 
				{
					if (!(ret % 6))
					puts("");
					printf("%.2X ", rxbuf [ret]);
				}
			}
			else 
			if  ( regId == 8 )
			{
				memcpy ( floatAsByte.byteVal, &rxbuf[0+4], SIZE_OF_FLOAT );
				yaw = floatAsByte.fVal;				
				gcvt(yaw, 6, productname);				
				printf ( "yaw = %s \n", productname );				
				memcpy ( floatAsByte.byteVal, &rxbuf[4+4], SIZE_OF_FLOAT );
				pitch = floatAsByte.fVal;
				gcvt(pitch, 6, productname);				
				printf ( "pitch = %s \n", productname );
				memcpy ( floatAsByte.byteVal, &rxbuf[8+4], SIZE_OF_FLOAT );
				roll = floatAsByte.fVal;
				gcvt(roll, 6, productname);				
				printf ( "roll = %s \n", productname );
				
				
				for (ret = 0; ret < responseSize  ; ret++) 
				{
					if (!(ret % 6))
						puts("");
					printf("%.2X ", rxbuf [ret]);
				}	
				puts("");
											
			}
			else 
			if  ( regId == 63 )
			{
				if (delay == 1)
				{
					memcpy ( floatAsByte.byteVal, &rxbuf[12+4], SIZE_OF_FLOAT );
					yaw = floatAsByte.fVal;				
					gcvt(yaw, 6, productname);				
					printf ( "yaw = %s \n", productname );				
					memcpy ( floatAsByte.byteVal, &rxbuf[16+4], SIZE_OF_FLOAT );
					pitch = floatAsByte.fVal;
					gcvt(pitch, 6, productname);				
					printf ( "pitch = %s \n", productname );
					memcpy ( floatAsByte.byteVal, &rxbuf[20+4], SIZE_OF_FLOAT );
					roll = floatAsByte.fVal;
					gcvt(roll, 6, productname);				
					printf ( "roll = %s \n", productname );
				
					memcpy ( &doubletobyte.arr[0], &rxbuf[24+4] , SIZE_OF_DOUBLE );
							
					snprintf(productname, sizeof(productname), "%g", doubletobyte.d);
					printf ( "latitude  = %s \n", productname );
				
					memcpy ( &doubletobyte.arr[0], &rxbuf[32+4] , SIZE_OF_DOUBLE );
								
					snprintf(productname, sizeof(productname), "%g", doubletobyte.d);
					printf ( "longtitude   = %s \n", productname );
				
					memcpy ( &doubletobyte.arr[0], &rxbuf[40+4] , SIZE_OF_DOUBLE );
							
					snprintf(productname, sizeof(productname), "%g", doubletobyte.d);
					printf ( "altutude  = %s \n", productname );		
				
				
					for (ret = 0; ret < responseSize  ; ret++) 
					{
						if (!(ret % 6))
							puts("");
						printf("%.2X ", rxbuf [ret]);
					}
				}				
				puts("");
			
				printf ( "." );
				
			}
			else
			if  ( regId == 58 )
			{
				memcpy ( &doubletobyte.arr[0], &rxbuf[16+4] , SIZE_OF_DOUBLE );							
				snprintf(productname, sizeof(productname), "%g", doubletobyte.d);
				printf ( "latitude  = %s \n", productname );				
				memcpy ( &doubletobyte.arr[0], &rxbuf[24+4] , SIZE_OF_DOUBLE );						
				snprintf(productname, sizeof(productname), "%g", doubletobyte.d);
				printf ( "longtitude   = %s \n", productname );
				
				memcpy ( &doubletobyte.arr[0], &rxbuf[32+4] , SIZE_OF_DOUBLE );
							
				snprintf(productname, sizeof(productname), "%g", doubletobyte.d);
				printf ( "altutude  = %s \n", productname );
				
				memcpy ( floatAsByte.byteVal, &rxbuf[40+4], SIZE_OF_FLOAT );
				NedVelX = floatAsByte.fVal;				
				gcvt(NedVelX, 6, productname);				
				printf ( "NedVelX = %s \n", productname );				
				memcpy ( floatAsByte.byteVal, &rxbuf[44+4], SIZE_OF_FLOAT );
				NedVelY = floatAsByte.fVal;
				gcvt(NedVelY, 6, productname);				
				printf ( "NedVelY = %s \n", productname );
				memcpy ( floatAsByte.byteVal, &rxbuf[48+4], SIZE_OF_FLOAT );
				NedVelZ = floatAsByte.fVal;
				gcvt(NedVelZ, 6, productname);				
				printf ( "NedVelZ = %s \n", productname );
				#ifdef DEBUG
				for (ret = 0; ret < responseSize  ; ret++) 
				{
					if (!(ret % 6))
						puts("");
					printf("%.2X ", rxbuf [ret]);
				}	
				puts("");
				#endif
			}
			#ifdef DEBUG
			else
			
				for (ret = 0; ret < responseSize  ; ret++) 
				{
					if (!(ret % 6))
						puts("");
					printf("%.2X ", rxbuf [ret]);
				}	
			puts("");
			#endif
		}
			
		
		usleep(delay * 10);
				
		times++;
		j++;
		if (delay > 1 )
			delay--;
		
		
		
	}
}
int main(void )
{

	int fd,  ret;
	char device_name[] = {"/dev/spidev2.0"};
	
	
	printf ( " opening %s \n", device_name );
	
	fd = open ( device_name , O_RDWR );
	if ( fd < 0)
	{
		printf ( " cannot open device %s \n", device_name );
		perror("failed to open spi device");
		abort();
	}
	
	/*
	 * spi mode
	*/
	
	mode = SPI_MODE_0; 
	mode |= SPI_CPOL;
	mode |= SPI_CPHA;
	  
	ret = ioctl(fd, SPI_IOC_WR_MODE, &mode);
	if (ret == -1)
		perror("can't set spi mode");

	/*
	 * bits per word
	 */
	ret = ioctl(fd, SPI_IOC_WR_BITS_PER_WORD, &bits);
	if (ret == -1)
		perror ("can't set bits per word");

	
	/*
	 * max speed hz
	 */
	ret = ioctl(fd, SPI_IOC_WR_MAX_SPEED_HZ, &speed);
	if (ret == -1)
		perror("can't set max speed hz");

	
	printf("spi mode: %d\n", mode);
	printf("bits per word: %d\n", bits);
	printf("max speed: %d Hz (%d KHz)\n", speed, speed/1000);

	transfer (fd );	
   	
	return 0;
}

int display_error(const char* msg)
{
	printf("%s\n", msg);
	return -1;
}

